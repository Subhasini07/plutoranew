Feature: BookingRequest

  @Web
  Scenario Outline: This scenario add the booking request
    Given Navigate to Plutora
    And Click on booking request and complete the request from <Excel>

    Examples:
      | Excel      |
      | BookingReq |
