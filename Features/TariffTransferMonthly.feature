Feature: TariffTransferMonthly


  @Web
  Scenario Outline: This scenario is to check whether Advisors are able change Tariff
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for MISDINS from <Excel> and navigates to DPA check and clears DPA to perform tariff transfer

    Examples:
      | Storeid | UserName       | Password  | Excel              |
      | 0001    | evoloadtest009 | Winter789 | TariffTransferData |
