Feature: Useraddition

  @Web
  Scenario Outline: This scenario add the existing user to the requested project.

    Given Navigate to ALM admin page
    And Enter <UserName> <Password> and click on login button to login to the application
    And Search for project from <Excel> and navigates to project and add the users with the resepctive role

    Examples:
      | UserName          | Password    | Excel    |
      | Subhasini.subbian | Monday@2019 | UserList |
