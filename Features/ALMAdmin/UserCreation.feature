Feature: UserCreation

  @Web
  Scenario Outline: This scenario to create an new user.

    Given Navigate to ALM admin page
    And Enter <UserName> <Password> and click on login button to login to the application
    #And Search for project from <Excel> and navigates to project and add the users with the resepctive role
    And Create a new user by getting the details form the <Excel> sheet


    Examples:
      | UserName          | Password    | Excel        |
      | Subhasini.Subbian | Monday@2019 | UserCreation |