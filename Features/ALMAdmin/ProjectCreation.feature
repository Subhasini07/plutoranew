Feature: ProjectCreation

  @Web
  Scenario Outline: This scenario is to create a new project.

    Given Navigate to ALM admin page
    And Enter <UserName> <Password> and click on login button to login to the application
    And Create a new <Project> <Domain> as per the request

    Examples:
      | UserName          | Password    | Project                    | Domain               |
      | Subhasini.Subbian | Monday@2019 | 46167_AO2_Data_Replication | DEFAULT_TELEFONICAUK |


#Project name should not be more that 30 characters