Feature: UserDeletion

  @Web
  Scenario Outline: This scenario to delete the list of users.

    Given Navigate to ALM admin page
    And Enter <UserName> <Password> and click on login button to login to the application
    #And Create a new user by getting the details form the <Excel> sheet
    And Delete the users by getting the details form the <Excel> sheet


    Examples:
      | UserName        | Password | Excel        |
      | Ravikumar.Singh | Sunday#1 | UserDeletion |