Feature: Test Data Validation

  @Web
  Scenario Outline: Validate the data
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for MISDINS from <Excel> and navigates to DPA check
    #And Enter <SecurityInfo> <SMSVerification> and click on Validate to clear DPA
    #Then Click on 'close' button on the top right corner of account tab to close the tab successfully


    Examples:
      | Storeid | UserName       | Password  | Excel    |
      | 0001    | evoloadtest009 | Winter789 | TestData |
