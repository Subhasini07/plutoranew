Feature: Evo_Postpay_Tariff_and_Bolton_Transfer

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able change Tariff
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    #When Account Dashboard gets displayed click on 'Tariff Transfer' link then select the tariff and save
    And Account Dashboard gets displayed click on 'Tariff Transfer' link then select the tariff and bolton and save
    And Click customer agrees to the Terms & Conditions Check Box and confirm then Bolton Change message will be displayed
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter456 | 0       | 1234            | 07801200082 |