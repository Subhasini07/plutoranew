Feature: RG49_Evo_Change_Access_Control_Postpay

  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to change access control field for a Postpay Customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Expand services field to select Access control
    And Select AccessControl <Option> to change and confirm
    Then Compares the value if changes has been reflected

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | Option         |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07521007404 | Default safety |
