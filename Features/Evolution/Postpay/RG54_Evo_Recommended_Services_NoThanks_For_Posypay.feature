Feature: Evo_Recommended_Services_NoThanks_For_Posypay

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that Recommended Services are available In Recommendations section for Pay Monthly Customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    Then Verify if recommendations are available and select nothanks option
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07703560990 |

