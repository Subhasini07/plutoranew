Feature: RG57_Evo_Customer_Contact_Method_Displayed_In_FCC_Customer_Present

  @Web
  Scenario Outline: Verify that Disclaimer Screen (Security Check screen) is displayed in FCC/CCC store when an Outbound Call option is selected from Customer Contact Method in FCC store
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen for FCC Customer
    Then Select Customer making incoming calland validate the customer details
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0827    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |