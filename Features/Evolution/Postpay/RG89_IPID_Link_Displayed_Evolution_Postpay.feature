Feature: RG89_IPID_Link_Displayed_Evolution_Postpay

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to search a MSISDIN and clear DPA Screen
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    Then click on IPID link and validate

    Examples:
      | Storeid | UserName       | Password | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | EvoWin09 | 0       | 1234            | 07801059003 |