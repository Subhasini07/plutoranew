Feature: RG44_Evo_Advisors able to change Address for a Prepay Customer

  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to edit the Address for a Prepay Customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on plus and edit to update the address
    And Give the <Postcode> and click on 'Search' Button and select 'address' form dropdown and click on 'Confirm'
    #Then Click on 'Close' Button to navigate back to Accout dashboard

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | Postcode |
      | 0001    | evoloadtest009 | Winter456 | 0       | 1234            | 07801055925 | LS110NE  |