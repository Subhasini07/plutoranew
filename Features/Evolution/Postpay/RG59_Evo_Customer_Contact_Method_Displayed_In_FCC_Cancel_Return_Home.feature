Feature: RG59_Evo_Customer_Contact_Method_Displayed_In_FCC_Cancel_Return_Home

  @Web
  Scenario Outline: Verify that cancel the call and return to home page
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen for FCC Customer
    Then Select cancel call option to navigate to home page


    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0827    | evoloadtest009 | Winter123 | 07801055925 |