Feature: RG46_Evo_Postpay_Bolton Change

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able change bolton
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    When Account Dashboard gets displayed click on 'Add_Remove Bolton' link then select the bolton and save
    And Click customer agrees to the Terms & Conditions Check Box and confirm then Bolton Change message will be displayed
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter456 | 0       | 1234            | 07801001663 |