Feature: RG58_Evo_FCC_CCC_Store_Outbound_Option_Selected_Customer_Contact_Method_FCC_Store

  @Web
  Scenario Outline: Verify that Disclaimer Screen (Security Check screen) is displayed in FCC/CCC store when an Outbound Call option is selected from Customer Contact Method in FCC store
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen for FCC Customer
    Then Select outbound option in FCC scren to proceed further


    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0827    | evoloadtest009 | winter123 | 07801055925 |