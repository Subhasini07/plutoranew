Feature: RG48_Evo_Postpay_SIM Swap

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to perform SIM Swap

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on Swap SIM and provide <SSN> to swap the SIM

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | SSN            |
      | 0001    | evoloadtest009 | Winter456 | 0       | 1234            | 07808011208 | 0065085287994  |