Feature: RG150_Validate_Handset_BillInfo_NoSecurity

  @Web
  Scenario Outline: To verify that When security answer isn’t known, but handset name is – entering the handset name is compulsory, and then entering any 1 from the remaining 2 financial questions - bank details, bill amount, (apart from photo ID which is mandatory) to clear postpay DPA check successfully. (applicable when NO option is selected in the SMS Verification of the postpay DPA screen).

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <Passport> number along with handset details <Handset> and <Billamt> and <Column> to clear DPA when Security information is not available

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | Billamt | Passport  | Handset                   | Column |
      | 0001    | evoloadtest009 | Winter456 | 07521113681 | 26.50   | 123456789 | Apple iPhone 5 16GB Black | 2      |