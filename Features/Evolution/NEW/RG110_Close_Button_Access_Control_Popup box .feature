Feature:RG110_Close_Button_Access_Control_Popup box

  @Web
  Scenario Outline: To verify that when the user clicks on thee, and a change has been successfully made, the "Cancel button" under the Access Control popup box changes to the "Close" button.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Expand services field to select Access control
    And Select AccessControl <Option> to change and confirm
    Then Compares the value if changes has been reflected

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | Option         |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801801145 | Default safety |
