Feature: RG100_SMS_Verification_Exactly4digit

  @Web
  Scenario Outline: To verify that the SMS code in the SMS Verification section is exactly 4 digits for postpay DPA check screens.
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
   # And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Enter <SMSVerification> and <BillAmt> and click on Validate the SMS code length
    Examples:
      | Storeid | UserName       | Password  | MSISDN      | BillAmt | SMSVerification |
      | 0001    | evoloadtest009 | Winter456 | 07568416879 | 0       | 1234            |