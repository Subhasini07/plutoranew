Feature: RG118_Password_Manager_VerifyAll_YES_NO_Option_Diplay

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that the user is not able to perform any other action without selecting Yes/No from the caution message dialog box (for Verify All button in Password Manager).

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Personal link' from Quicklink dropdown
    And select <Option> from 'Personal' dropdown and Confirm navigation to <Option> Page
    And Verify if the error message is validated on clicking verify all option


    Examples:
      | Storeid | UserName       | Password  | Option           |
      | 0001    | evoloadtest009 | Winter123 | Password Manager |