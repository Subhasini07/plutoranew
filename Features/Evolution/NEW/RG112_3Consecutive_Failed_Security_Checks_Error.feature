Feature: RG135_Security_Information_Section_Validation

  @Web
  Scenario Outline: To verify that on the Consumer Postpay DPA screen under Security Information section advisor can enter any two letters from  the customer's security answer as valid input to access consumer dashboard along with the SMS Verification section

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    #And Enter <SecInfo> and security information to Clear the DPA
    And Enter <SecInfo> <MSISDN> and security information for consicutive 3 times to check the failure

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SecInfo |
      | 0001    | evoloadtest009 | Winter123 | 07568415908 | James   |