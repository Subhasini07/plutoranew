Feature: RG114_Number_Attempts_Remaining

  @Web
  Scenario Outline: To verify that when the user fails to clear the DPA check for a prepay/postpay MSISDN,  the counter at the top of the screen should reflect the number of attempts remaining for the advisor to successfully clear the DPA check.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
   #And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Enter <SMSVerification> and <BillAmt> and click on Validate and check the remaining attempts

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | BillAmt | SMSVerification |
      | 0001    | evoloadtest009 | Winter123 | 07568416879 | 26.50   | 1234            |