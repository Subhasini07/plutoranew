Feature: RG145_PhotoId_Information_NotAvailable_OnClicking_Yes_After_NoOption

  @Web
  Scenario Outline: To verify that when the advisor selects the NO option from the SMS Verification section (in the initial postpay DPA check screen) and proceeds with entering the Photo ID information and other details, but selects YES option before clicking on validate, then only the Photo ID information should be deleted from the text boxes. All remaining entries should stay in case the user wants to proceed the journey ahead with the YES answer.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <Passport> <Billamt> number along with security information  <SecInfo> and Bank details <Sortcode> <AccNo> to Verify PhotId section and then clear DPA when selected NO option

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SecInfo | Passport  | Sortcode | AccNo    | Billamt |
      | 0001    | evoloadtest009 | Winter123 | 07801058148 | James   | 123456789 | 774914   | 70422162 | 26.50   |