Feature: RG102.3_Validate_PhotID_UKDriving_Licence

  @Web
  Scenario Outline: To verify that when all 4 answers are known - the advisor can request 1 financial and 1 non-financial question, (along with photo ID section) to clear postpay DPA check successfully. (applicable when NO option is selected in the SMS Verification of the postpay DPA screen).

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter photoID details  <selectedvalue> and <IDREference> number along with <SecInfo> and <Billamt> to clear DPA when selected NO option

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SecInfo | Billamt | selectedvalue      | IDREference        |
      | 0001    | evoloadtest009 | Winter123 | 07801058148 | James   | 26.50   | UK Driving Licence | MUNIR803108LK9MR |