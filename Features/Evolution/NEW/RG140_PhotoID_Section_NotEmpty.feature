Feature: RG140_PhotoID_Section_NotEmpty

  @Web
  Scenario Outline: To verify that the Name field under Photo ID section, in postpay DPA check screen, does not accept a blank value and the corresponding name entered in the field is displayed as the same in the Photo ID validation popup box.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Validate the name field and then Enter <Passport> number along with <SecInfo> and <Billamt> and click on validate


    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SecInfo | Billamt | Passport  |
      | 0001    | evoloadtest009 | Winter123 | 07801058148 | James   | 26.50   | 123456789 |