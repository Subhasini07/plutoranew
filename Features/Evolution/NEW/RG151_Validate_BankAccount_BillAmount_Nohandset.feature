Feature: RG151_Validate_BankAccount_BillAmount_Nohandset

  @Web
  Scenario Outline: To verify that When neither handset name nor security answer is known – the customer should answer the only 2 remaining - bank details, bill amount, (along with photo ID) to clear postpay DPA check successfully. (applicable when NO option is selected in the SMS Verification of the postpay DPA screen).

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <Passport> number along with financial details <Billamt> <Sortcode> <AccNo> when no handset detail to clear DPA when selected NO option
    Examples:
      | Storeid | UserName       | Password  | MSISDN      | Billamt | Passport  | Sortcode | AccNo    |
      | 0001    | evoloadtest009 | Winter456 | 07521116156 | 0       | 123456789 | 774914   | 70422162 |