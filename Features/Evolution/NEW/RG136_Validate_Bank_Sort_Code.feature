Feature: RG136_Validate_Bank_Sort_Code

  @Web
  Scenario Outline: To verify that on the Consumer Postpay DPA screen the advisor can enter bank sort code of the following 3 to access consumer dashboard when YES option is selected and security answer for the MSISDN is not available

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
   #And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Enter <Sortcode> <AccNo> and SMS to Clear the DPA

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | Sortcode | AccNo    |
      | 0001    | evoloadtest009 | Winter456 | 07568415908 | 774914   | 70422162 |