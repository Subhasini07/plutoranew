Feature: RG104_Capatalize_PhotoID

  @Web
  Scenario Outline: To verify that all the characters entered under ID Reference under Photo ID section (in postpay DPA check screen) are automatically capitalized to make it more readable.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <Passport> <SecInfo> and <Billamt> number to check if the alphabets are capatalize

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | Billamt | Passport  | SecInfo |
      | 0001    | evoloadtest009 | Winter456 | 07568421607 | 0       | abcdefghi | test    |
