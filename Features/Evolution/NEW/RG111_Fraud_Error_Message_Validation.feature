Feature: RG153_Validate_SecurityInfo_BillInfo_NO

  @Web
  Scenario Outline: To verify that if the security answer for a prepay or postpay MSISDN contains the word "Fraud", then the DPA check screen should show the message - "You are not authorised to deal with this account. Please contact 0344 847 1420 on behalf of your customer" and the customer details are not shown.


    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Check if the error is dispalyed for Fraud customer

    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07703990753 |