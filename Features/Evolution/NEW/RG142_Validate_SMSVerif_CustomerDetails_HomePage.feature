Feature: RG153_Validate_SecurityInfo_BillInfo_NO

  @Web
  Scenario Outline: To verify that when the advisor searches for a postpay MSISDN, only the Customer Details and SMS Verification sections are displayed initially in the DPA check screen.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    #And Enter <Passport> number along with <SecInfo> and <Billamt> to clear DPA when selected NO
    And Validate only SMS and Customer details displayed in Account page

    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07801058148 |