Feature: RG108_Validate_NO_Option_Popup_Control_Edit

  @Web
  Scenario Outline: To verify that if the advisor clicks on the No button, or the 'x' button, of the "Photo ID validation - please confirm" popup box, the advisor is able to edit the values entered in the Photo ID section under postpay DPA check screen.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    #And Enter <Passport> number along with <SecInfo> and <Billamt> to clear DPA when selected NO
    And Enter <Passport> number along with <SecInfo> and <Billamt> and click on validate snd select no and try editing


    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SecInfo | Billamt | Passport  |
      | 0001    | evoloadtest009 | Winter123 | 07801058148 | James   | 26.50   | 123456789 |