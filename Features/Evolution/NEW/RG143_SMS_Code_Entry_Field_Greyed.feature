Feature: RG143_SMS_Code_Entry_Field_Greyed

  @Web
  Scenario Outline: To verify that the SMS code entry field and the Validate button are greyed out until the customer selects the YES/NO radio button. If No is selected, the SMS code entry fields are greyed out still then.
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
   # And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Validate if the SMS code Validate button are greyed
    Examples:
      | Storeid | UserName       | Password  | MSISDN      | BillAmt | SMSVerification |
      | 0001    | evoloadtest009 | Winter123 | 07568416879 | 0       | 1234            |