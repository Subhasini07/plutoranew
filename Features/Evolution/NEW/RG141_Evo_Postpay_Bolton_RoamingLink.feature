Feature: RG141_Evo_Postpay_Bolton_RoamingLink

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that hyperlinks are added to the Roaming page to assist the user, in the Boltons tab.The link "Click here to find out more" is displayed, clicking on which takes the user to the URL - https://www.o2.co.uk/international/internationalhub, and the roaming page is to be hosted in Jacada Application Holder.
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And validate if the roaming Link is present in section
    #When Account Dashboard gets displayed click on 'Add_Remove Bolton' link then select the bolton and save
    #And Click customer agrees to the Terms & Conditions Check Box and confirm then Bolton Change message will be displayed
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 26.50   | 1234            | 07801058148 |