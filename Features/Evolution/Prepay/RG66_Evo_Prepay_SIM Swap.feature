Feature: RG66_Evo_Prepay_SIM Swap

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to perform SIM Swap for prepay customer

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    And Click on Swap SIM and provide <SSN> <Passport> to swap the SIM

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | SSN           |Passport|
      | 0001    | evoloadtest009 | Winter123 | 07751003170 | 0066450700298 |123456789|

