Feature: RG94_Popup_Message_Searches_Postpay_MPN_After_First_Failed_DPA_Prepay

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check pop up is visible after first attempt of DPA is failed
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 6789 and Check if pop up appers after first failed attempt


    Examples:
      | Storeid | UserName       | Password| MSISDN      |
      | 0001    | evoloadtest009 | Winter123| 07346999350 |