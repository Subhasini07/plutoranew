Feature: RG70_Evo_Recommended_Services_NoThanks_For_Prepay

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify whether advisor is able to delete services from Recommendations section by clicking No Thanks option

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    Then Verify if recommendations are available and select nothanks option for prepay
    #Then Navigate to Account information

    Examples:
      | Storeid | UserName   | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07751003270|

