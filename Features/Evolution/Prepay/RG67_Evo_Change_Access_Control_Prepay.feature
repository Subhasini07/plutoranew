Feature: RG67_Evo_Change_Access_Control_Prepay

  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to change access control field for a Prepay Customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    And Expand services field to select Access control
    And Select AccessControl <Option> to change and confirm
    Then Compares the value if changes has been reflected

    Examples:
      | Storeid | UserName       | Password | MSISDN      | Option         |
      | 0001    | evoloadtest009 | Winter123 | 07346999343 | Default safety |
