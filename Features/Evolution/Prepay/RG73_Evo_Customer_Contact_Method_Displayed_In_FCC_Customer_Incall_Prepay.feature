Feature: RG73_Evo_Customer_Contact_Method_Displayed_In_FCC_Customer_Incall_Prepay

  @Web
  Scenario Outline: Verify that Disclaimer Screen (Security Check screen) is displayed in FCC/CCC store when an Outbound Call option is selected from Customer Contact Method in FCC store
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen for FCC Customer
    Then Select Customer making incoming calland validate the customer details
    And Enter 1234 and click on Validate

    Examples:
      | Storeid | UserName   | Password  | MSISDN      |
      | 0827    | evoloadtest009 | Winter123 | 07751003172 |