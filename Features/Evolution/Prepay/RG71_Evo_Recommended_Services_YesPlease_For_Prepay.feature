Feature: RG71_Evo_Recommended_Services_YesPlease_For_Prepay

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify whether advisor is able to services from Recommendations section by clicking Yes Please option
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    Then Verify if recommendations are available and select yesPlease option for Prepay
    #Then Navigate to Account information

    Examples:
      | Storeid | UserName   | Password  | MSISDN     |
      | 0001    |  evoloadtest009|  Winter123| 07751003270 |

