Feature: Evo_Customer_Contact_Method_Displayed_In_FCC_Customer_Present_Prepay

  @Web
  Scenario Outline: Verify that Customer is present in store and validate the account
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen for FCC Customer
    Then Select Customer present and validate the customer details
    And Enter 1234 and click on Validate

    Examples:
      | Storeid | UserName   | Password  | MSISDN      |
      | 0827    |  evoloadtest009| Winter123 | 07751003172 |