Feature: RG61_62_63Prepay_MSISDN_DPA_Pass

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to search a MSISDIN and clear DPA Screen
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    #Then Navigate to Account information


    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07751003176 |