Feature: RG65_Evo_Prepay_Tariff_Transfer

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able change Tariff
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    When Account Dashboard gets displayed click on 'Tariff Transfer' link then select the tariff and save for prepay
    #And Click customer agrees to the Terms & Conditions and confirm to change the tariff
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07346999353 |