Feature: RG69_Evo_Recommended_Services_Available_Recommendations_Section_For_Prepay

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that Recommended Services are available In Recommendations section for Prepay Customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    Then Validate if recommendations are available for Prepay
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07751003170 |

