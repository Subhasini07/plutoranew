Feature: RG64_Evo_Prepay_Bolton Change

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able change bolton
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter 1234 and click on Validate
    When Account Dashboard gets displayed click on 'Add_Remove Bolton' link then select the bolton for prepay and save
    And Click customer agrees to the Terms & Conditions Check Box and confirm then Bolton Change message will be displayed
   #Then Navigate to Account information

    Examples:
      | Storeid | UserName       | Password | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07346999367 |