Feature: RG28_Access_HouseKeeping_PCMT_Evolution

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open PCMT page in Evolution
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Housekeeping link' from Quicklink dropdown
    And select <Option> from 'Housekeeping' dropdown and Confirm navigation to <Option> Page


    Examples:
      | Storeid | UserName       | Password | Option                     |
      | 0001    | evoloadtest009 | EvoWin09 | PCMT Reconciliation Report |