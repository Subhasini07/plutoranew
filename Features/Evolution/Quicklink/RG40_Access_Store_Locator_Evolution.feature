Feature: RG40_Access_Store_Locator_Evolution
  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Storage Locator
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And Select 'Service link' from Quicklink dropdown
    And Select <Option> from 'service' dropdown and Confirm navigation to <Option> Page

    Examples:
      | Storeid | UserName       | Password | Option        |
      | 0001    | evoloadtest009 | EvoWin09 | Store Locator|


