Feature: RG23_Access_Sales_Company_Directory_ID_Check_Page_Evolution

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Company Directory ID check Page
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Sales link' from Quicklink dropdown
    And select <Option> from 'Sales' dropdown and Confirm navigation to <Option> Page


    Examples:
      | Storeid | UserName       | Password | Option                    |
      | 0001    | evoloadtest009 | EvoWin09 | Company Director ID Check |