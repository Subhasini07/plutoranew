Feature: RG99_Access_Services_Address_Finder_Evolution

  #launch hooks and get browsert
  @Web
  Scenario Outline: Successfully open address finder in Evolution
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And Select 'Service link' from Quicklink dropdown
    And Select <Option> from 'service' dropdown and Confirm navigation to <Option> Page


    Examples:
      | Storeid | UserName       | Password | Option  |
      | 0001    | evoloadtest009 | EvoWin09 | Address |