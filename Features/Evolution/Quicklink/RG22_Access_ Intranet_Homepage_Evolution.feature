Feature: RG22_Access_Intranet_Homepage_Evolution

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Intranet Homepage
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And search <text> and navigate back to intranet home page

    Examples:
      | Storeid | UserName       | Password | text   |
      | 0001    | evoloadtest009 | EvoWin09 | Awards |
