Feature: RG33_Access_Personal_Email_Page_Evolution

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Intranet Email Page
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Personal link' from Quicklink dropdown
    And select <Option> from 'Personal' dropdown and Confirm navigation to <Option> Page


    Examples:
      | Storeid | UserName       | Password | Option     |
      | 0001    | evoloadtest009 | EvoWin09 | Email (O2) |