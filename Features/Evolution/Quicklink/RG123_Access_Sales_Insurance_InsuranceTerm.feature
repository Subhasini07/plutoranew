Feature: RG123_Access_Sales_Insurance_InsuranceTerm

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Insurance Term in new tab
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Sales link' from Quicklink dropdown
    And Select 'Insurance' from Quicklink dropdown
    And select <Option> from 'Sales' dropdown and Confirm navigation to <Option> Page

    Examples:
      | Storeid | UserName       | Password  | Option          |
      | 0001    | evoloadtest009 | Winter123 | Insurance Terms |