Feature: RG12_Previous_Customer_Details_Not_Displayed_Next_Customer

  @Web
  Scenario Outline: To verify that whether previous customer details are not displayed while accessing for next customer.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    Then Click on 'Next customer' button  to navigate to home page
    And Search for New <MSISDN1> and Provide <BillAmt> <SMSVerification> clear DPA and verify data

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | MSISDN1     |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 | 07568416879 |