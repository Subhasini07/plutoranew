Feature: RG14_Evo_Unable_Navigate_Previous_Page_Pressing_Backspace_key_keyboard

  @Web
  Scenario Outline: To verify that the advisor is not able to navigate to the previous page  by pressing 'Backspace' key in keyboard after logging into Evolution.
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Clicking on backspace to not able to navigate to the previous page from evolution

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |