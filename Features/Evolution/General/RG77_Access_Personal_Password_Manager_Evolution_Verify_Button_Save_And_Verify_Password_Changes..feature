Feature: RG77_Access_Personal_Password_Manager_Evolution_Verify_Button_Save_And_Verify_Password_Changes

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Password Manager
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on 'Quicklink' dropdown
    And select 'Personal link' from Quicklink dropdown
    And select <Option> from 'Personal' dropdown and Confirm navigation to <Option> Page
    And Change password with <PMUname> <PMPassword> for the field and verify

    Examples:
      | Storeid | UserName       | Password  | Option           | PMUname    | PMPassword      |
      | 0001    | evoloadtest009 | Winter123 | Password Manager | GFRONT1@O2 | my02u4tpa55w0rd |