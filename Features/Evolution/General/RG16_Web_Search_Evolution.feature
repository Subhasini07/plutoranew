Feature: RG18_RG18_Web_Search_Evolution

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that the advisor is able perform a web search

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Perform a <web> search and validate


    Examples:
      | Storeid | UserName       | Password  | web        |
      | 0001    | evoloadtest009 | Winter123 | google.com |