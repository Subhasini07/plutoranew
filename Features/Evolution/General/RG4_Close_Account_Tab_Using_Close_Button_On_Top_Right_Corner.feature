Feature: RG4_Close_Account_Tab_Using_Close_Button_On_Top_Right_Corner

  @Web
  Scenario Outline: Successfully  Closed the account tab using the close button
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    Then Click on 'close' button on the top right corner of account tab to close the tab successfully


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |