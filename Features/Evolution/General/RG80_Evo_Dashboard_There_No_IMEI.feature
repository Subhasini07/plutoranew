Feature: RG80_Evo_Dashboard_There_No_IMEI

  @Web
  Scenario Outline: To validate the handset details section of the dashboard where there is no IMEI, it should states “No IMEI” on account.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Check if the IMEI are available in Handset field
   #Then Compares the value if changes has been reflected

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07521007404 |
