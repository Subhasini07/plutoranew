Feature: RG3_Lock_and_Unlock_Evolution


  #launch hooks and get browser
  @Web
  Scenario Outline: Unlocked Evolution Screen Successfully.
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Click on lock button to lock Screen
    Then Screen should be locked succesfully
    And Provide <Password> and click on login

    Examples:
      | Storeid | UserName       | Password |
      | 0001    | evoloadtest009 | Winter123 |