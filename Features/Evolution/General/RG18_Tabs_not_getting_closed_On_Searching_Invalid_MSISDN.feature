Feature: RG16_Tabs_not_getting_closed_On_Searching_Invalid_MSISDN

  @Web
  Scenario Outline: To verify that whether the tabs are not getting closed while searching for an invalid MSISDN.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on contact history on account dashboard
    And Search for New <MSISDN1> all the opened tabs will not get closed


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      | MSISDN1 |
      | 0001    | evoloadtest009 | Winter123| 0       | 1234            | 07801055925 | 0780808 |