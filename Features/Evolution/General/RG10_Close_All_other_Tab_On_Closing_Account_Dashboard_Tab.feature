Feature: RG10_Close_All_other_Tab_On_Closing_Account_Dashboard_Tab

  @Web
  Scenario Outline: To verify that the new tabs opened tabs are getting closed while closing Account tab for a prepay/postpay MSISDN
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on contact history on account dashboard
    Then Click on 'close' button on the top right corner of account tab to close the tab successfully


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |