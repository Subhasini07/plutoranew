Feature: RG86_Evo_Original_Connection_O2_Displayed_MMYYYY_Format

  @Web
  Scenario Outline: This scenario is to check whether Advisors are able see the original connection field
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Expand the account information and validate the Original connection field


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |