Feature: RG21_Contact_History_Detail_Enlarge_Form

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to checl the contack details in Enlarge form
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Navigate to Contact history page to view the details

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |