Feature: RG2_Logout_Evolution
  This scenario ensures that when the Agent in acquisition selects a 'Dongle' and tariff with single promotion, then the 'Promotions' section with all applied promotion description should be displayed in the deal builder

  #launch hooks and get browser
  @Web
  Scenario Outline: Logout of evolution
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And click on logout to logout from application


    Examples:
      | Storeid | UserName       | Password |
      | 0001    | evoloadtest009 | Winter456 |