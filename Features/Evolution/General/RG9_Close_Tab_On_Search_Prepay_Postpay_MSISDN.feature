Feature: RG9_Close_Tab_On_Search_Postpay_MSISDN

  @Web
  Scenario Outline: Successfully closed all the tabs on search of new MSISDN
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Open the QuickLinks
    And Search for <MSISDN> and navigates to DPA check screen

    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07801055925 |