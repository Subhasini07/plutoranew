Feature:RG96_Automatically_logged_Salesforce_Performing_Business_Upgrade

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to search a MSISDIN and clear DPA Screen
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    Then Validate saleforce


    Examples:
      | Storeid | UserName       | Password  | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 07518377361 |