Feature: RG52_Access_Latest_Statement

  #launch hooks and get browser
  @Web
  Scenario Outline: This scenario is to check whether Advisors are able to access Latest statement
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on latest statement and validate

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123| 0       | 1234            | 07801055925 |