Feature: RG1_Login_Evolution


  #launch hooks and get browser
  @Web
  Scenario Outline: Login to the evolution
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button

    Examples:
      | Storeid | UserName       | Password |
      | 0001    | evoloadtest009 | Winter123 |