Feature: RG8_Evo_Tabs_Opened_Tabs_Getting_Closed_Clicking_Next Customer_Postpay

  @Web
  Scenario Outline: Successfully closed all the tabs on clicking next customer
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Click on contact history on account dashboard
    And Click on 'Next customer' button  to navigate to home page


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07801055925 |