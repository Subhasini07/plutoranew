Feature: RG7_Evo_Account_Dashboard_Closed_Next_Customer_Postpay

  @Web
  Scenario Outline: To verify that the  account dashboard is closed  when the advisor clicks on the Next customer button on the postpay MSISDN
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    Then Click on 'Next customer' button  to navigate to home page


    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | Evoloadtest001 | Winter123 | 0       | 1234            | 07801055925 |