Feature: RG95_ Remedy_link_ unsuccessful_MPN

  #launch hooks and get browser
  @Web
  Scenario Outline: Successfully open Remedy - IT Helpdesk link in Evolution
    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for  invalid <MSISDN> and navigate to remedy
    And Click on 'Quicklink' dropdown
    And select 'Housekeeping link' from Quicklink dropdown
    And select <Option> from 'Housekeeping' dropdown and Confirm navigation to <Option> Page


    Examples:
      | Storeid | UserName       | Password  | Option               | MSISDN |
      | 0001    | evoloadtest009 | Winter123 | Remedy - IT Helpdesk | 0754   |

