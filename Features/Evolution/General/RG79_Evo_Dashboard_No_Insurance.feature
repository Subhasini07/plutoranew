Feature: RG79_Evo_Dashboard_No_Insurance

  @Web
  Scenario Outline: To validate the handset details section of the dashboard where there is no insurance, it should states “No insurance” on account.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Check if the Insurance are available in Handset field
   #Then Compares the value if changes has been reflected

    Examples:
      | Storeid | UserName       | Password  | BillAmt | SMSVerification | MSISDN      |
      | 0001    | evoloadtest009 | Winter123 | 0       | 1234            | 07568413728 |

