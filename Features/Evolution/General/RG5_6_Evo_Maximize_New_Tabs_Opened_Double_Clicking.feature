Feature: RG5_6_Evo_Maximize_New_Tabs_Opened_Double_Clicking

  #launch hooks and get browser
  @Web
  Scenario Outline: To verify that the advisor is able to maximize and minimize the new tabs opened within Evolution to full screen by double clicking on the tab.

    Given Navigate to evolution application
    And Enter <Storeid> <UserName> <Password> and click on login button
    And Search for <MSISDN> and navigates to DPA check screen
    And Enter <SMSVerification> and <BillAmt> and click on Validate to clear DPA
    And Select the application and maximize and minimize to check the functionality

    Examples:
      | Storeid | UserName       | Password  | MSISDN      | BillAmt | SMSVerification |
      | 0001    | evoloadtest009 | Winter123 | 07801055925 | 0       | 1234            |