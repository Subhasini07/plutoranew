package actionsPerformed;

import helpers.Environment;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import pageobjects.AdminPage;
import pageobjects.ProjectPage;

public class projectPageAction extends Environment{


    public static void projectCreation(String Project, String Domain) throws Throwable {

        System.out.println("the project needs to be created is"+Project);
        System.out.println("the project needs to be created in the domain "+Domain);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Project);
        Thread.sleep(7000);
        driver.switchTo().frame("rightPaneFrame");
        System.out.println("successfull switched the frame");
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.pjImg);
        Thread.sleep(2000);
        driver.switchTo().frame("exeConsolePage");
        System.out.println("successfull switched the frame exeConsolePage");
        Thread.sleep(2000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.tmptPJ);
        Thread.sleep(2000);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("rightPaneFrame");
        Thread.sleep(2000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.next);
        Thread.sleep(2000);
        driver.switchTo().frame("exeConsolePage");
        int count = driver.findElements(By.xpath("//table[@id='createproj_s3']//tr[@id='ADMINISTRATION'] ")).size();
        System.out.println("print count===="+count);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.adminstrator);
        Thread.sleep(2000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.adminstratorPJ);
        Thread.sleep(2000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.adminstratorPJTmpt);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("rightPaneFrame");
        if(ProjectPage.next.isEnabled())
        {
            ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.next);
        }
        driver.switchTo().frame("exeConsolePage");
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.selectAll);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("rightPaneFrame");
        if(ProjectPage.next.isEnabled())
        {
            ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.next);
        }
        driver.switchTo().frame("exeConsolePage");
        ProjectPage.pjName.sendKeys(Project);
        Thread.sleep(2000);
        Select domain = new Select(driver.findElement(By.id("createProject_domainName")));
        domain.selectByVisibleText(Domain);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.chkPJ);
        driver.switchTo().defaultContent();
        driver.switchTo().frame("rightPaneFrame");
        Thread.sleep(2000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.finish);
        Thread.sleep(3000);
        driver.switchTo().frame("exeConsolePage");
        Thread.sleep(3000);
        String popup = driver.findElement(By.xpath("//span[normalize-space()='Project must be deactivated']")).getText();
        System.out.println("the popup vsalue is"+popup);
        if(popup.equals("Project must be deactivated"))
        {
            ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.OK);
            Thread.sleep(15000);
        }
        else
        {
            Assert.fail("failed to create project");
        }
        Thread.sleep(8000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  ProjectPage.OK);
    }
}
