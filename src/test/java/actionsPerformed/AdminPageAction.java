package actionsPerformed;

import gherkin.lexer.Th;
import helpers.Environment;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.AdminPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class AdminPageAction extends Environment {

    public static String projectName="";
    public static String LoginName="";
    public static String FullName="";
    public static String Password="";
    public static String Email="";
    public static String Description="";
    public static String userName="";
    //public static int count=1;
    public static  int rowCount;
    public static  int value;
    public static String deleteLoginID;
    public static  String[] uN = new String[rowCount];
    public static  String[] pN = new String[rowCount];
    public static  int[] val = new int[rowCount];

    //public static String [][] data1 = new String[][];
    public static int data;

    public static void navigateToAdminPage() throws Throwable {
        Thread.sleep(3000);
        String geturl = driver.getCurrentUrl();
        String gettitle= driver.getTitle();
        System.out.println("geturl" + geturl);
        System.out.println("gettitle" + gettitle);
        String popup = AdminPage.msg.getText();
        System.out.printf("the popup value is "+popup);
        Thread.sleep(2000);
        if(popup.equalsIgnoreCase( "Warning"))
        {
               AdminPage.msgOk.click();
               Thread.sleep(2000);
        }
        else if(popup.equalsIgnoreCase( "Error Occured"))
        {
            AdminPage.msgOk.click();
            Thread.sleep(2000);
        }
    }
    public static void enterCredentialsToLogin(String UserName, String Password) throws Throwable {

        AdminPage.uname.isDisplayed();
        System.out.println("the value is displayed");
       AdminPage.uname.sendKeys(UserName);
        Thread.sleep(3000);
        AdminPage.pwd.sendKeys(Password);
        Thread.sleep(3000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Login);
        Thread.sleep(3000);
        System.out.println("Succesfully Logged");
        if(AdminPage.loginError.isDisplayed())
        {
            Assert.fail("Invalid credentials helce failed");
        }
        else
        {
            System.out.println("succesfully logged into the page");
        }
    }

    public static void searchAndAddUserName(String excelDoc) throws Throwable {

        String filePath = System.getProperty("user.dir")+"\\Configurations\\testData\\"+excelDoc+".xlsx";             //Prepare the path of excel file
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Project);
        Thread.sleep(5000);
        readExcelUseraddition(filePath);        //Call read file method of the class to read data

    }


/*
     public static void readExcelUseraddition(String filePath) throws Throwable {

        File file = new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook ALMWorkbook = new XSSFWorkbook(inputStream);

        XSSFSheet ALMSheet = ALMWorkbook.getSheetAt(0);   //Read sheet inside the workbook by its name
        int rowCount = ALMSheet.getLastRowNum();    //Find number of rows in excel file
        int cellCount = ALMSheet.getRow(0).getLastCellNum();

        projectName = ALMSheet.getRow(1).getCell(0).getStringCellValue().trim();
        System.out.println("\n Project Name is" + projectName);

        driver.switchTo().frame("rightPaneFrame");
        Thread.sleep(2000);
        AdminPage.Search.sendKeys(projectName);
         AdminPage.Search.sendKeys(Keys.RETURN);
        Thread.sleep(3000);

        if (AdminPage.userTab.isDisplayed()) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.userTab);
            Thread.sleep(3000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.addUser);
            Thread.sleep(3000);
            driver.switchTo().frame("exeConsolePage");
            Thread.sleep(3000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.advancSearch);
            Thread.sleep(2000);
            driver.switchTo().frame("exeConsolePage");
            for (int i = 1; i <= rowCount; i++) {
                userName = ALMSheet.getRow(i).getCell(1).getStringCellValue().trim();
                System.out.println("The user name is\n" + userName);
                Thread.sleep(3000);
                AdminPage.filterList.sendKeys(userName);
                Thread.sleep(3000);
                AdminPage.filterList.sendKeys(Keys.RETURN);
            }
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            driver.switchTo().frame("exeConsolePage");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.filter);
            Thread.sleep(3000);
            AdminPage.allUser.click();
            Thread.sleep(2000);
            AdminPage.allUser.sendKeys(Keys.chord(Keys.CONTROL, "a"));
            Thread.sleep(2000);
            AdminPage.moveRight.click();
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.next);
            Thread.sleep(2000);
            driver.switchTo().frame("exeConsolePage");
            for (int i = 1; i <= rowCount; i++) {
                exeName = ALMSheet.getRow(i).getCell(1).getStringCellValue().trim();
                System.out.println("the value is" + exeName);
                value = (int) ALMSheet.getRow(i).getCell(3).getNumericCellValue();
                System.out.println("The Value to be selected in the excel is\n" + value);
                Thread.sleep(2000);
                WebElement myxpath = driver.findElement(By.xpath("//input[@loginname='" + exeName + "' and @value='" + value + "']"));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", myxpath);
                Thread.sleep(2000);
                System.out.println("selected the required role");
            }
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            Thread.sleep(10000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.finish);
            Thread.sleep(15000);
            if (AdminPage.close.isEnabled()) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.close);
                Thread.sleep(2000);
            } else {
                Assert.fail("Failed the script becasue unable to find the close button");
            }
        }
        else
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.querypopupOk);
            Assert.fail("failed due to unable to find the project");
        }
    }
*/


   public static void readExcelUseraddition(String filePath) throws Throwable {

        File file = new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook ALMWorkbook = new XSSFWorkbook(inputStream);

        XSSFSheet ALMSheet = ALMWorkbook.getSheetAt(0);   //Read sheet inside the workbook by its name
        int count=1;
        int rowCount = ALMSheet.getLastRowNum();    //Find number of rows in excel file
        int cellCount = ALMSheet.getRow(0).getLastCellNum();

        for(int i=1;i<=rowCount;i++) {
            projectName = ALMSheet.getRow(i).getCell(0).getStringCellValue().trim();
            System.out.println("\n Project Name is" + projectName);
            driver.switchTo().frame("rightPaneFrame");
            Thread.sleep(2000);
            AdminPage.Search.sendKeys(projectName);
            AdminPage.Search.sendKeys(Keys.RETURN);
            Thread.sleep(2000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.userTab);
            Thread.sleep(3000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.addUser);
            Thread.sleep(3000);
            driver.switchTo().frame("exeConsolePage");
            Thread.sleep(3000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.advancSearch);
            Thread.sleep(2000);
            driver.switchTo().frame("exeConsolePage");
            Thread.sleep(3000);
            System.out.println("The count vlaue is "+count);
            for(int j=count+1;j<=count+1;j++) {
                System.out.println("the value of count is first \n" + count);
                System.out.println("the value of j is" + j);
                projectName = ALMSheet.getRow(j).getCell(0).getStringCellValue().trim();
                if (projectName.isEmpty()) {
                    userName = ALMSheet.getRow(j).getCell(1).getStringCellValue().trim();
                    System.out.println("The user name is\n" + userName);
                    Thread.sleep(3000);
                    AdminPage.filterList.sendKeys(userName);
                    Thread.sleep(3000);
                    AdminPage.filterList.sendKeys(Keys.RETURN);
                    count++;
                    System.out.println("count value is" + count);
                    j = count;
                }
                else
                {
                    break;
                }
            }
            i=count-1;
            System.out.println("\n the value of i is "+i);
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            driver.switchTo().frame("exeConsolePage");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.filter);
            Thread.sleep(3000);
            AdminPage.allUser.click();
            Thread.sleep(2000);
            AdminPage.allUser.sendKeys(Keys.chord(Keys.CONTROL, "a"));
            Thread.sleep(2000);
            AdminPage.moveRight.click();
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.next);
            Thread.sleep(2000);
            driver.switchTo().frame("exeConsolePage");
           // driver.switchTo().frame("exeConsolePage");
            int count1=1;
            for (int k = count1+1; k <= count; k++) {
               String exeName = ALMSheet.getRow(k).getCell(1).getStringCellValue().trim();
                System.out.println("the value is" + exeName);
                value = (int) ALMSheet.getRow(k).getCell(3).getNumericCellValue();
                System.out.println("The Value to be selected in the excel is\n" + value);
                Thread.sleep(2000);
                WebElement myxpath = driver.findElement(By.xpath("//input[@loginname='" + exeName + "' and @value='" + value + "']"));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", myxpath);
                Thread.sleep(2000);
                System.out.println("selected the required role");
            }
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            Thread.sleep(10000);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.finish);
            Thread.sleep(15000);
            if (AdminPage.close.isEnabled()) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.close);
                Thread.sleep(2000);
            } else {
                Assert.fail("Failed the script becasue unable to find the close button");
            }
            driver.switchTo().defaultContent();
            System.out.println("count value is"+count);
            System.out.println("count value of i is"+i);
        }
        System.out.println("count value is"+count);
   }


    public static void writeExcelBoltonftransfer(String accTariffResult,int currentRow, String filePath) throws IOException {
        File file =    new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook evoWorkbook  = new XSSFWorkbook(inputStream);

        XSSFSheet evoSheet = evoWorkbook.getSheetAt(0);   //Read sheet inside the workbook by its name

        evoSheet.getRow(currentRow).createCell(1).setCellValue(accTariffResult);

        inputStream.close(); //Close input stream

        FileOutputStream outputStream = new FileOutputStream(file);  //Create an object of FileOutputStream class to create write data in excel file

        evoWorkbook.write(outputStream); //write data in the excel file
        //close output stream
        outputStream.close();

    }

    public static void createNewUser(String excelDoc) throws Throwable {

     String filePath = System.getProperty("user.dir")+"\\Configurations\\testData\\"+excelDoc+".xlsx";             //Prepare the path of excel file
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Project);
        Thread.sleep(5000);
        readExcelUserCreate(filePath);        //Call read file method of the class to read data

    }

    public static void readExcelUserCreate(String filePath) throws Throwable {

        File file =    new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook ALMCreate  = new XSSFWorkbook(inputStream);

        XSSFSheet ALMCreateSheet = ALMCreate.getSheetAt(0);   //Read sheet inside the workbook by its name

        int rowCount = ALMCreateSheet.getLastRowNum();    //Find number of rows in excel file
        int cellCount = ALMCreateSheet.getRow(0).getLastCellNum();

        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.userItem);

        driver.switchTo().frame("rightPaneFrame");
        Thread.sleep(2000);

        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Lcheck);
        Thread.sleep(2000);

        for(int i=1;i<=rowCount;i++)
        {
            System.out.println("rowcout"+rowCount);
           // driver.switchTo().frame("rightPaneFrame");
            Thread.sleep(3000);
            ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.newUserItem);
            Thread.sleep(2000);

            FullName = ALMCreateSheet.getRow(i).getCell(0).getStringCellValue().trim();
            System.out.println("\n Login Name is" + FullName);
            AdminPage.fNmae.sendKeys(FullName);
            Thread.sleep(2000);

            Password = ALMCreateSheet.getRow(i).getCell(1).getStringCellValue().trim();
            System.out.println("\n password is" +Password );
            AdminPage.uPassword.sendKeys(Password);
            Thread.sleep(2000);

            Email = ALMCreateSheet.getRow(i).getCell(2).getStringCellValue().trim();
            System.out.println("\n Emalid is" + Email);
            AdminPage.email.sendKeys(Email);
            Thread.sleep(2000);

            String[] str1=Email.split("@");
            for(int j=0;j<1; j++) {
                System.out.println("the value is" + str1[0]);
                LoginName = str1[0];
                System.out.println("the value after spliting is"+LoginName);
                AdminPage.loginID.sendKeys(LoginName);
                Thread.sleep(2000);
            }
            Description = ALMCreateSheet.getRow(i).getCell(3).getStringCellValue().trim();
            System.out.println("\n Organisation is" + Description);
            AdminPage.organsation.sendKeys(Description);
            Thread.sleep(2000);

            ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.uOK);
            System.out.println("user"+FullName+"successfully created");
          //  driver.switchTo().defaultContent();
            Thread.sleep(3000);
        }
    }
    public static void deleteUsers(String excelDoc) throws Throwable {

        String filePath = System.getProperty("user.dir")+"\\Configurations\\testData\\"+excelDoc+".xlsx";             //Prepare the path of excel file
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",  AdminPage.Project);
        Thread.sleep(5000);
        readExcelDeleteUsers(filePath);        //Call read file method of the class to read data

    }

    public static void readExcelDeleteUsers(String filePath) throws Throwable {

        File file = new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook ALMDelete = new XSSFWorkbook(inputStream);

        XSSFSheet ALMDeleteSheet = ALMDelete.getSheetAt(0);   //Read sheet inside the workbook by its name
        int rowCount = ALMDeleteSheet.getLastRowNum();    //Find number of rows in excel file
        int cellCount = ALMDeleteSheet.getRow(0).getLastCellNum();

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.userItem);
        driver.switchTo().frame("rightPaneFrame");

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.adminFilter);
        Thread.sleep(2000);
        driver.switchTo().frame("exeConsolePage");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.userfilter);
        for (int i = 1; i <= rowCount; i++) {
            String deleteLoginID = ALMDeleteSheet.getRow(i).getCell(0).getStringCellValue().trim();
            AdminPage.userfilter.sendKeys(deleteLoginID);
            Thread.sleep(2000);
            AdminPage.filterList.sendKeys(Keys.RETURN);
        }
        driver.switchTo().defaultContent();
        driver.switchTo().frame("rightPaneFrame");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.filter);
        Thread.sleep(2000);
        for (int j = 1; j <= rowCount; j++) {
            driver.switchTo().defaultContent();
            driver.switchTo().frame("rightPaneFrame");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.deleteUser);
            Thread.sleep(2000);
            String str1 = AdminPage.deleteOkcheck.getText();
            if (str1.equals("Delete User")) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.deleteOk);
                Thread.sleep(8000);
            } else {
                Assert.fail("Script failed to delete the user" + deleteLoginID);
            }
            Thread.sleep(2000);
            driver.switchTo().defaultContent();
            String str2 = AdminPage.deleteCloseCheck.getText();
            if (str2.equals("Finished")) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", AdminPage.deleteClose);
                Thread.sleep(2000);
            } else {
                Assert.fail("Script failed to delete the user" + deleteLoginID);
            }
        }
    }

   }
