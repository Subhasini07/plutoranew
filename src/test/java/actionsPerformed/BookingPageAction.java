package actionsPerformed;

import gherkin.lexer.Th;
import helpers.Environment;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageobjects.BookingPage;

import java.awt.print.Book;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Driver;

public class BookingPageAction extends Environment {

    public static void navigateToPlutora() throws Throwable {
        Thread.sleep(3000);
        String geturl = driver.getCurrentUrl();
        String gettitle= driver.getTitle();
        System.out.println("geturl" + geturl);
        System.out.println("gettitle" + gettitle);
    }
    public static void enterCredentialsToLogin(String Email, String Password) throws Throwable {

        BookingPage.email.isDisplayed();
        BookingPage.email.sendKeys(Email);
        Thread.sleep(3000);
        BookingPage.password.sendKeys(Password);
        Thread.sleep(3000);
        System.out.println("waiting to complete the recaptha");
        WebDriverWait wait = new WebDriverWait(driver,40);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@id='nav__0__1']/div")));
        Thread.sleep(3000);
        System.out.println("Succesfully Logged");
    }
    public static void CompleteBookingForm(String excel)throws Throwable
    {

        String filePath = System.getProperty("user.dir")+"\\Configurations\\testData\\"+excel+".xlsx";             //Prepare the path of excel file
        Thread.sleep(5000);
        readExcelBooking(filePath);
    }

    public static void readExcelBooking(String filePath) throws Throwable {

        File file = new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook BookingRequest = new XSSFWorkbook(inputStream);

        XSSFSheet BookingRequestSheet = BookingRequest.getSheetAt(0);   //Read sheet inside the workbook by its name

        int rowCount = BookingRequestSheet.getLastRowNum();    //Find number of rows in excel file

        WebElement cli = driver.findElement(By.xpath("(//span[normalize-space()='Booking Request'])[3]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", cli);
       // cli.click();
        Thread.sleep(2000);

        for (int i = 1; i <= rowCount; i++) {

            BookingPage.entertxt.clear();
            BookingPage.entertxt.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
             String title = BookingRequestSheet.getRow(i).getCell(0).getStringCellValue().trim();
            BookingPage.entertxt.sendKeys(title);
            BookingPage.entertxt.sendKeys(Keys.RETURN);
            int num = driver.findElements(By.xpath("//div[@class='x-grid-empty']")).size();
            if (num >= 1) {

                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.booking);
                Thread.sleep(2000);

                String title1 = BookingRequestSheet.getRow(i).getCell(0).getStringCellValue().trim();
                BookingPage.title.sendKeys(title1);
                System.out.println("The valus of status" + title1);

                String Desc = BookingRequestSheet.getRow(i).getCell(1).getStringCellValue().trim();
                BookingPage.description.sendKeys(Desc);

                String Type = BookingRequestSheet.getRow(i).getCell(2).getStringCellValue().trim();
                //System.out.println("The valus of status" + Type);

                if (Type.equalsIgnoreCase("Known Issue")) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.type);
                    Thread.sleep(2000);
                    BookingPage.type.sendKeys(Keys.DOWN);
                    BookingPage.type.sendKeys(Keys.RETURN);
                    Thread.sleep(2000);
                } else if (Type.equalsIgnoreCase("Project Testing")) {
                    {
                        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.type);

                        BookingPage.type.sendKeys(Keys.DOWN);
                        BookingPage.type.sendKeys(Keys.DOWN);
                        BookingPage.type.sendKeys(Keys.RETURN);
                        Thread.sleep(2000);
                    }
                } else if (Type.equalsIgnoreCase("UnPlanned Change")) {
                    {
                        System.out.println("Selected Unplanned Issue");
                    }
                }

                String sdate = BookingRequestSheet.getRow(i).getCell(4).getStringCellValue().trim();
                //System.out.println("\n the start date from the excel =====" + sdate);
                BookingPage.startdate.sendKeys(sdate);
                Thread.sleep(3000);

                String edate = BookingRequestSheet.getRow(i).getCell(5).getStringCellValue().trim();
               // System.out.println("\n the enddate from the excel is" + edate);
                BookingPage.enddate.sendKeys(edate);
                Thread.sleep(2000);
                //((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.description);
                BookingPage.description.click();
                Thread.sleep(3000);
                String assign = BookingRequestSheet.getRow(i).getCell(6).getStringCellValue().trim();
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.assignee);
                 if (assign.equalsIgnoreCase("Refsupport")) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.assignee);
                    for(int f=0;f<5;f++) {
                        Thread.sleep(1000);
                        BookingPage.assignee.sendKeys(Keys.DOWN);
                    }
                    BookingPage.assignee.sendKeys(Keys.RETURN);
                }
                if (assign.equalsIgnoreCase("PlutoraTestEnvSupp")) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.assignee);
                    for(int f=0;f<3;f++) {
                        Thread.sleep(1000);
                        BookingPage.assignee.sendKeys(Keys.DOWN);
                    }
                    BookingPage.assignee.sendKeys(Keys.RETURN);
                }
                //System.out.println("\nThe assignee value is " + assign);
                Thread.sleep(2000);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.checkTest);
                Thread.sleep(2000);
                String projectDuration = BookingRequestSheet.getRow(i).getCell(7).getStringCellValue().trim();
                BookingPage.Pjdur.sendKeys(projectDuration);
               // System.out.println("\nThe Project Duration value is " + projectDuration);

                String Outage = BookingRequestSheet.getRow(i).getCell(8).getStringCellValue().trim();
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.outage);
                BookingPage.outage.sendKeys(Outage);
                BookingPage.outage.sendKeys(Keys.TAB);
               // System.out.println("\nThe Outage value is " + Outage);

                String Downtime = BookingRequestSheet.getRow(i).getCell(9).getStringCellValue().trim();
                BookingPage.downtime.sendKeys(Downtime);
               // System.out.println("\nThe Downtime value is " + Downtime);

                String ProjectNumber = BookingRequestSheet.getRow(i).getCell(10).getStringCellValue().trim();
                BookingPage.PJ.sendKeys(ProjectNumber);
               // System.out.println("\nThe ProjectNumber value is " + ProjectNumber);

                String Requestor = BookingRequestSheet.getRow(i).getCell(11).getStringCellValue().trim();
                BookingPage.requestor.sendKeys(Requestor);
               // System.out.println("\nThe Requestor value is " + Requestor);

                String RootCause = BookingRequestSheet.getRow(i).getCell(12).getStringCellValue().trim();
                BookingPage.rootcause.sendKeys(RootCause);
                //System.out.println("\nThe Root Cause value is " + RootCause);

                String SoleUse = BookingRequestSheet.getRow(i).getCell(13).getStringCellValue().trim();
                BookingPage.soleUse.sendKeys(SoleUse);
                BookingPage.soleUse.sendKeys(Keys.TAB);
                //System.out.println("\nThe sole use value is " + SoleUse);

                String SysAffected = BookingRequestSheet.getRow(i).getCell(14).getStringCellValue().trim();
                if (SysAffected.equalsIgnoreCase("Incident")) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.assignee);
                    for( int a=0;a<9;a++){
                    BookingPage.systemaffected.sendKeys(Keys.DOWN);}
                    BookingPage.systemaffected.sendKeys(Keys.RETURN);
                }
                //System.out.println("\nThe system affecteed value is " + SysAffected);

                String TestHours = BookingRequestSheet.getRow(i).getCell(15).getStringCellValue().trim();
                BookingPage.testLost.sendKeys(TestHours);
                //System.out.println("\nThe test Lost hours value is " + TestHours);

                String WoNum = BookingRequestSheet.getRow(i).getCell(16).getStringCellValue().trim();
                BookingPage.woNum.sendKeys(WoNum);
                //System.out.println("\nThe Wo Num value is " + WoNum);

                String testPhase = BookingRequestSheet.getRow(i).getCell(17).getStringCellValue().trim();
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.testingPhase);
                BookingPage.testingPhase.sendKeys(testPhase);
                BookingPage.testingPhase.sendKeys(Keys.TAB);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.grpName);

                int envnum = BookingRequestSheet.getRow(i).getLastCellNum();
                //System.out.println("enum value ========"+envnum);
                for(int k=18;k<envnum;k++) {
                    String GroupEnv = BookingRequestSheet.getRow(i).getCell(k).getStringCellValue().trim();
                    System.out.println("\n Group environment value is" + GroupEnv);
                    BookingPage.liveSearch.clear();
                    BookingPage.liveSearch.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
                    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.liveSearch);
                    BookingPage.liveSearch.sendKeys(GroupEnv);
                    Thread.sleep(2000);
                    BookingPage.liveSearch.sendKeys(Keys.RETURN);
                    Thread.sleep(3000);
                    if(GroupEnv.isEmpty())
                    {
                        System.out.println("the coulmn is empty");
                    }
                    else {
                        if (GroupEnv.contains("SOA AG")) {
                            WebElement soa = driver.findElement(By.xpath("(//div/a[normalize-space()='SOA AG']//preceding::div[normalize-space()='Name'])[2]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(soa, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.equals("BMC patrol")) {
                            WebElement srm = driver.findElement(By.xpath("//div/a[normalize-space()='BMC patrol - Netcool']//preceding::div[normalize-space()='Name']"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(2000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(srm, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("CPSR")) {
                            WebElement MDCOM = driver.findElement(By.xpath("(//div/a[normalize-space()='CPSR']//preceding::div[normalize-space()='Name'])[1]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(2000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(MDCOM, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("SRM FT2")) {
                            WebElement sandvine = driver.findElement(By.xpath("(//div/a[normalize-space()='SRM FT2']//preceding::div[normalize-space()='Name'])[2]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(2000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(sandvine, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("eComm")) {
                            WebElement opp = driver.findElement(By.xpath("(//div/a[normalize-space()='eComm']//preceding::div[normalize-space()='Name'])[20]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(opp).clickAndHold().moveByOffset(0, 2000).release(opp).build().perform();
                            Thread.sleep(2000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(opp, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("Ref Administration Services")) {
                            WebElement refa = driver.findElement(By.xpath("(//div/a[normalize-space()='Ref Administration Services']//preceding::div[normalize-space()='Name'])[6]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(refa).clickAndHold().moveByOffset(0, 650).release(refa).build().perform();
                            Thread.sleep(2000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(refa, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("Reference Network")) {
                            WebElement opp = driver.findElement(By.xpath("(//div/a[contains(text(),'Reference Network')]//preceding::div[normalize-space()='Name'])[4]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(opp).clickAndHold().moveByOffset(0, 500).release(opp).build().perform();
                            Thread.sleep(5000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(opp, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("Micros")) {
                            WebElement mic = driver.findElement(By.xpath("(//div/a[contains(text(),'Micros')]//preceding::div[normalize-space()='Name'])[39]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(mic).clickAndHold().moveByOffset(0, 40000).release(mic).build().perform();
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(mic, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.equals("SMP")) {
                            WebElement orange = driver.findElement(By.xpath("(//div/a[normalize-space()='SMP']//preceding::div[normalize-space()='Name'])[2]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(orange, dropenv).build().perform();
                            Thread.sleep(4000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("Transact")) {
                            WebElement opp = driver.findElement(By.xpath("(//div/a[contains(text(),'Transact')]//preceding::div[normalize-space()='Name'])[1]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(opp, dropenv).build().perform();
                            Thread.sleep(2000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("RRD")) {
                            WebElement opp = driver.findElement(By.xpath("(//div/a[contains(text(),'RRD')]//preceding::div[normalize-space()='Name'])[2]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(opp, dropenv).build().perform();
                            Thread.sleep(2000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.contains("Identity")) {
                            WebElement opp = driver.findElement(By.xpath("(//div/a[contains(text(),'Identity')]//preceding::div[normalize-space()='Name'])[3]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(opp).clickAndHold().moveByOffset(0, 40000).release(opp).build().perform();
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(opp, dropenv).build().perform();
                            Thread.sleep(8000);
                            System.out.println("the environment was dropped");
                        } else if (GroupEnv.equals("Messaging")) {
                            WebElement orange = driver.findElement(By.xpath("(//div/a[normalize-space()='Messaging']//preceding::div[normalize-space()='Name'])[3]"));
                            WebElement dropenv = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(5000);
                            Actions dragger = new Actions(driver);
                            dragger.moveToElement(orange).clickAndHold().moveByOffset(0, 40000).release(orange).build().perform();
                            Thread.sleep(5000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(orange, dropenv).build().perform();
                            Thread.sleep(5000);
                            System.out.println("the environment was dropped");
                        } else {
                            System.out.println(" updated it");
                            Thread.sleep(2000);
                            WebElement sourceLocator = driver.findElement(By.xpath("(//div/a[normalize-space()='"+GroupEnv+"']//preceding::div[normalize-space()='Name'])[1]")); //to be use for same column
                            WebElement targetLocator = driver.findElement(By.xpath("//span[normalize-space()='Drop Environment Here']"));
                            Thread.sleep(3000);
                            Actions action = (Actions) new Actions(driver);
                            action.dragAndDrop(sourceLocator, targetLocator).build().perform();
                            Thread.sleep(2000);
                        }
                    }
                }
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.save);
                Thread.sleep(2000);
                int sub = driver.findElements(By.xpath("//div[@class='plt__main__body']/div/div[5]/ul/li/a")).size();
                System.out.println("The value is "+sub);
                if(sub>=1)
                {
                    Assert.fail("Failed because of blank field");
                }
                else
                {
                    System.out.println("Success");
                }
                //This for loop is to change the status of the script
            for(int j=0;j<3;j++) {
                Thread.sleep(1000);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.status);
                BookingPage.status.sendKeys(Keys.DOWN);
                BookingPage.status.sendKeys(Keys.RETURN);
                Thread.sleep(2000);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.save);
                Thread.sleep(1000);
                }
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.cls);
                //Thread.sleep(5000);
                String status="Uploaded";
               // writeStatus(i,filePath,status);
            }
            else
            {
                String status="Duplicate";
                writeStatus(i,filePath,status);
            }
            Thread.sleep(2000);
        }
           //this for loop is to approve the environment
        WebElement clic = driver.findElement(By.xpath("(//span[normalize-space()='Booking Request'])[3]"));
        clic.click();
        Thread.sleep(3000);
        for(int k=1;k<=rowCount;k++)
        {
            BookingPage.entertxt.clear();
            BookingPage.entertxt.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
            String title1 = BookingRequestSheet.getRow(k).getCell(0).getStringCellValue().trim();
            BookingPage.entertxt.sendKeys(title1);
            BookingPage.entertxt.sendKeys(Keys.RETURN);
            Thread.sleep(3000);
            WebElement linktxt = driver.findElement(By.xpath("(//div[@class='x-grid-item-container'])[2]//tr/td[3]/div/div[normalize-space()='"+title1+"']"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", linktxt);
            Thread.sleep(3000);
            String textPen =BookingPage.bookingPen.getText();
            //System.out.println("\nTextValue=="+textPen);
            if(textPen.equalsIgnoreCase("Pending")) {
                Actions dragger = new Actions(driver);
               // dragger.moveToElement(BookingPage.bookingPen).clickAndHold().moveByOffset(0, 1000).release(BookingPage.bookingPen).build().perform();
                dragger.moveToElement(BookingPage.bookingPen).moveByOffset(0, 1000).release(BookingPage.bookingPen).build().perform();
                 ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.bookingPen);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.approv);
                 ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.appSaveClose);
                Thread.sleep(2000);
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", BookingPage.saveClose);
                Thread.sleep(3000);
            }
        }
    }
    public static void writeStatus( int currentRow, String filePath,String stat) throws IOException {

        File file = new File(filePath);  //Create an object of File class to open xlsx file

        FileInputStream inputStream = new FileInputStream(file); //Create an object of FileInputStream class to read excel file

        XSSFWorkbook sam = new XSSFWorkbook(inputStream);

        XSSFSheet sheet1 = sam.getSheetAt(0);   //Read sheet inside the workbook by its name

        sheet1.getRow(currentRow).createCell(3).setCellValue(stat);

        inputStream.close(); //Close input stream
        FileOutputStream outputStream = new FileOutputStream(file);  //Create an object of FileOutputStream class to create write data in excel file
        sam.write(outputStream); //write data in the excel file
        System.out.println("Writteen in Excel");
        //close output stream
        outputStream.close();
    }
}
