package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProjectPage {


    @FindBy(how= How.XPATH,using="//div[@id='projectsItem']")
    public static WebElement Project;

    @FindBy(how= How.XPATH,using="//div[@id='toolbar']/div[2]/img")
    public static WebElement pjImg;

    @FindBy(how= How.XPATH,using="//table[@id='createproj_s1']/tbody/tr[5]/td/nobr/input")
    public static WebElement tmptPJ;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Next>']")
    public static WebElement next;

    @FindBy(how= How.XPATH,using="//table[@id='createproj_s3']//tr[@id='ADMINISTRATION']/td/nobr/img")
    public static WebElement adminstrator;

    @FindBy(how= How.XPATH,using="//table[@id='createproj_s3']//tr[@id='ADMINISTRATION']//tr[@id='ADMINISTRATION.Projects']/td/nobr/img")
    public static WebElement adminstratorPJ;

    @FindBy(how= How.XPATH,using="//tr[@id='ADMINISTRATION.Projects.MODEL_PROJECT_1']/td/nobr/span")
    public static WebElement adminstratorPJTmpt;

    @FindBy(how= How.XPATH,using="//button[normalize-space()='Select All']")
    public static WebElement selectAll;

    @FindBy(how= How.XPATH,using="//input[@id='createProject_projectName']")
    public static WebElement pjName;

    @FindBy(how= How.XPATH,using="//select[@id='createProject_domainName']")
    public static WebElement domainName;

    @FindBy(how= How.XPATH,using="//div[@id='divTemplateLinkageCheckBox']/input")
    public static WebElement chkPJ;

    @FindBy(how= How.XPATH,using="//button/span[normalize-space()='Finish']")
    public static WebElement finish;

    @FindBy(how= How.XPATH,using="//body[@class='yui-skin-sam']/div[@id='main']/./following-sibling::div[4]/div[3]//span[normalize-space()='OK']")
    public static WebElement OK;
}
