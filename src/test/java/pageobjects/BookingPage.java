package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BookingPage {

    @FindBy(how= How.XPATH,using="//input[@id='username']")
    public static WebElement email;

    @FindBy(how= How.XPATH,using="//input[@id='password']")
    public static WebElement password;

    @FindBy(how= How.XPATH,using="//span[@id='recaptcha-anchor']")
    public static WebElement checkbox;

    @FindBy(how= How.XPATH,using="//button[@value='Login']")
    public static WebElement login;

    @FindBy(how= How.XPATH,using="//li[@id='nav__0__6']/div")
    public static WebElement New;

    @FindBy(how= How.XPATH,using="//span[contains(text(),'+ New Booking Request')]")
    public static WebElement booking;

    @FindBy(how= How.XPATH,using="//input[@name='Summary']")
    public static WebElement title;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Type']//parent::label//following-sibling::div//input")
    public static WebElement type;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Status']//parent::label//following-sibling::div/div/div/input")
    public static WebElement status;

    @FindBy(how= How.XPATH,using="//input[contains(@id,'datetimefield')][1]")
    public static WebElement startdate;

    @FindBy(how= How.XPATH,using="(//input[contains(@id,'datetimefield')]//parent::div)[2]/input")
    public static WebElement enddate;

    @FindBy(how= How.XPATH,using="//iframe[contains(@id,'htmleditor')][1]")
    public static WebElement description;

    @FindBy(how= How.XPATH,using="//label[contains(@id,'avatarSingleSelectComboWithLoca')]//following-sibling::div/div/div//input")
    public static WebElement assignee;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-panel')]//div[normalize-space()='Root Cause'])[5]//following-sibling::div/div/div/div//div/textarea")
    public static WebElement rootcause;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-panel')]//div[contains(text(),'project duration')]/parent::div)[1]/following-sibling::div//input")
    public static WebElement Pjdur;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[1]//div/input")
    public static WebElement outage;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[8]//div/input")
    public static WebElement soleUse;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[3]//div/input")
    public static WebElement downtime;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[9]//div/input")
    public static WebElement systemaffected;

        @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[10]//div/input")
    public static WebElement testLost;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[6]//div/textarea")
    public static WebElement requestor;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-panel')]//div[normalize-space()='Project Number'])/parent::div/following-sibling::div//textarea")
    public static WebElement PJ;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[5]//div/textarea")
    public static WebElement woNum;

    @FindBy(how= How.XPATH,using="//div[contains(@class,'bookingEnvBlock ')]//input")
    public static WebElement grpName;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Save']")
    public static WebElement save;

    @FindBy(how= How.XPATH,using="//a/span/span/span[normalize-space()='Save & Close']")
    public static WebElement saveClose;

    @FindBy(how= How.XPATH,using="//input[contains(@id,'liveFilterTebrEnvironment')]")
    public static WebElement liveSearch;

    @FindBy(how= How.XPATH,using="//div[@class='x-grid-empty']")
    public static WebElement match;

    @FindBy(how= How.XPATH,using="//div[@class='booking-btn']")
    public static WebElement bookingBtn;

    @FindBy(how= How.XPATH,using="//div[@class='booking-btn']/following-sibling::div[2]")
    public static WebElement bookingPen;

    @FindBy(how= How.XPATH,using="//label/b[normalize-space()='Approved']")
    public static WebElement approv;

    @FindBy(how= How.XPATH,using="(//span[normalize-space()='Save & Close'])[2]")
    public static WebElement appSaveClose;

    @FindBy(how= How.XPATH,using="//div[@id='columnBookingTitle-titleEl']/following-sibling::div//div//input")
    public static WebElement entertxt;

    @FindBy(how= How.XPATH,using="(//div[contains(@class,'x-container container-editor')])[11]//input")
    public static WebElement testingPhase;

    @FindBy(how= How.XPATH,using="//div/label[normalize-space()='Additional Information']")
    public static WebElement checkTest;

    @FindBy(how= How.XPATH,using="//div[@class='x-tool x-box-item x-tool-default x-tool-after-title']/img")
    public static WebElement cls;

    @FindBy(how= How.XPATH,using="//div[@class='x-panel custom-field-panel x-panel-default']/div/div/div[2]")
    public static WebElement wrk;

   // (//div[@class='x-grid-item-container'])[2]//tr/td[3]/div/div[normalize-space()='Test31']


}
