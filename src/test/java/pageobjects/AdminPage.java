package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class AdminPage {

    @FindBy(how= How.XPATH,using="//div[6]/div/span")
    public static WebElement msg;

    @FindBy(how= How.XPATH,using="//div[6]/div[3]/button/span")
    public static WebElement msgOk;

    @FindBy(how= How.XPATH,using="//input[@id='userName']")
    public static WebElement uname;

    @FindBy(how= How.XPATH,using="//input[@id='password']")
    public static WebElement pwd;

    @FindBy(how= How.XPATH,using="//button[@id='buttonLogin']")
    public static WebElement Login;

    @FindBy(how= How.XPATH,using="//span[contains(text(),'Error has Occurred')]")
    public static WebElement loginError;

    @FindBy(how= How.XPATH,using="//div[@id='projectsItem']")
    public static WebElement Project;

    @FindBy(how= How.XPATH,using="//input[@id='searchFilter']")
    public static WebElement Search;

    @FindBy(how= How.XPATH,using="//li[@name='users']/a")
    public static WebElement userTab;

    @FindBy(how= How.XPATH,using="//div[@id='add_to_project_button']")
    public static WebElement addUser;

    @FindBy(how= How.XPATH,using="//button[@id='freetext_button']")
    public static WebElement advancSearch;

    @FindBy(how= How.XPATH,using="//textarea[@id='itemsList']")
    public static WebElement filterList;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Filter']")
    public static WebElement filter;

    @FindBy(how= How.XPATH,using="//select[@id ='all_users']")
    public static WebElement allUser;

    @FindBy(how= How.XPATH,using="//div[@class ='buttonarea']")
    public static WebElement moveRight;

    @FindBy(how= How.XPATH,using="//span[normalize-space()='Next>']")
    public static WebElement next;

    @FindBy(how= How.XPATH,using="//button/span[normalize-space()='Finish']")
    public static WebElement finish;

    @FindBy(how= How.XPATH,using="//div[@id='main']//div[@id='pbDiv']/div[normalize-space()='Operation Complete']")
    public static WebElement completed;

    @FindBy(how= How.XPATH,using="//button/span[normalize-space()='Finish']/../preceding-sibling::button/span\n")
    public static WebElement close;

    @FindBy(how= How.XPATH,using="//div[@id='userItem']")
    public static WebElement userItem;

    @FindBy(how= How.XPATH,using="//div[@class='toolbar']/div[@id='add_user_button']/img")
    public static WebElement newUserItem;

    @FindBy(how= How.XPATH,using="//input[@id='userNameNew']")
    public static WebElement loginID;

    @FindBy(how= How.XPATH,using="//input[@id='fullNameNew']")
    public static WebElement fNmae;

    @FindBy(how= How.XPATH,using="//input[@id='passwordNew']")
    public static WebElement uPassword;

    @FindBy(how= How.XPATH,using="//input[@id='emailNew']")
    public static WebElement email;

    @FindBy(how= How.XPATH,using="//input[@id='descriptionNew']")
    public static WebElement organsation;

    @FindBy(how= How.XPATH,using="//input[@id='sendNotification']")
    public static WebElement Lcheck;

    @FindBy(how= How.XPATH,using="//body[@class='yui-skin-sam']/div[16]/div[3]/button[2]/span")
    public static WebElement uOK;

    @FindBy(how= How.XPATH,using="//th[@id='yui_h1']//td/img[2]")
    public static WebElement adminFilter;

    @FindBy(how= How.XPATH,using="//textarea[@id='itemsList']")
    public static WebElement userfilter;

    @FindBy(how= How.XPATH,using="//body/div[7]/div/div//tr[2]/td[2]")
    public static WebElement querypopup;

    @FindBy(how= How.XPATH,using="//body/div[7]/div[3]/button/span")
    public static WebElement querypopupOk;

    @FindBy(how= How.XPATH,using="//div[@id='delete_user_button']/img")
    public static WebElement deleteUser;

    @FindBy(how= How.XPATH,using="//div[18]/div[3]/button[2]/span")
    public static WebElement deleteOk;

    @FindBy(how= How.XPATH,using="//div[18]/div/span")
    public static WebElement deleteOkcheck;

    @FindBy(how= How.XPATH,using="//div[@id='pbDiv']//button[2]")
    public static WebElement deleteClose;

    @FindBy(how= How.XPATH,using="//div[@id='pbDiv']//div/div[1]")
    public static WebElement deleteCloseCheck;



}
