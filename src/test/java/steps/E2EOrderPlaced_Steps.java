package steps;

import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import actionsPerformed.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pageobjects.*;

public class E2EOrderPlaced_Steps {

    private static final String Filteroption = null;
    public WebDriver driver;
    public List<HashMap<String, String>> datamap;
    String FilterDataOption = null;
    LinkedList<String> expectedListBeforeSort = null;
    LinkedList<String> originalList = null;
    ArrayList<Integer> originalTariffList = null;
    ArrayList<Integer> expectedTariffListBeforeSort = null;
    LinkedList<String> TempList3 = null;
    String DataFilterRange = null;
    ArrayList<Integer> datalistbefore = new ArrayList<Integer>();
    ArrayList<Integer> datalistafter = new ArrayList<Integer>();
    ArrayList<Integer> monthlycostlistafter = new ArrayList<Integer>();
    ArrayList<Integer> upfrontcostlistafter = new ArrayList<Integer>();
    final static Logger log = Logger.getLogger("E2EOrderPlaced_Steps");
    static int BuyOutValue = 0;

    public E2EOrderPlaced_Steps() {
        driver = Hooks.driver;
        // datamap = DataReader.data();

    }

    @Given("^Navigate to Plutora$")
    public void navigate_to_AdminPage() throws Throwable {
        driver.manage().timeouts().implicitlyWait(140, TimeUnit.SECONDS);
        PageFactory.initElements(driver, BookingPage.class);
        BookingPageAction.navigateToPlutora();
    }
    @And("^Enter ([^\"]*) ([^\"]*) and click on login button$")
    public void enter_Credentials_to_Login(String Email, String Password) throws Throwable {
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        PageFactory.initElements(driver, BookingPage.class);
        BookingPageAction.enterCredentialsToLogin(Email, Password);
    }
    @And("^Click on booking request and complete the request from ([^\"]*)$")
    public void Complete_booking_form(String excel) throws Throwable {
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
        PageFactory.initElements(driver, BookingPage.class);
        BookingPageAction.CompleteBookingForm(excel);
    }


}
